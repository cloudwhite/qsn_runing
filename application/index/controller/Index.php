<?php
namespace app\index\controller;


use think\Db;

class Index extends \think\Controller
{
    //首页
    public function index()
    {
        //活动类型
        $activity_typ= db('activity_type')->where('status =0')->field('id,title')->select();
        //活动区域
        $activity_area= get_activty_area();
        //项目
        $activity_data = db('activity_cms')->where('status = 1 and is_audit =1')->field('id,title_img,title,ctime')->order('id desc')->limit(5)->select();
        //留言板
        $guestbook =  Db::table('qsn_guestbook')->alias('a')->join('qsn_user b','a.user_id=b.id')->where('a.status > 0')->field('a.id,a.content,b.name,b.head_img')->limit(5)->select();
        //活动掠影
        $cms = db('cms')->where('status >0')->field('id,title,title_img')->limit(10)->select();
        //推荐项目
        $show_data = db('activity_cms')->where('status=1 and is_audit =1 and is_show=1')->field('id,title_img,title,ctime')->order('id desc')->limit(5)->select();
        $this->assign('cms', $cms);
        $this->assign('guestbook', $guestbook);
        $this->assign('activity_type', $activity_typ);
        $this->assign('activity_show', $show_data);
        $this->assign('activity_area', $activity_area);
        $this->assign('activity_data', $activity_data);
        return $this->fetch('index');
    }

    //联系我们
    public function address(){
        echo 11111;
    }
}
