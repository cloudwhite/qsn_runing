<?php

namespace app\index\controller;

use think\Controller;
use think\Request;

class Activity extends Controller
{


    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch('index');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function detail()
    {

        $id = Request::instance()->param('id');

        return $this->fetch('detail');
    }

    //活动搜索结果
    public function search(){
        $activity_type = Request::instance()->param('activity_type');
        $activity_area = Request::instance()->param('activity_area');
        $activity_status = Request::instance()->param('activity_status');
        $activity_name = Request::instance()->param('activity_name');
        $now_time = time();
        if($activity_area){
            $where['area_id'] = $activity_area;
        }
        if($activity_type){
            $where['type_id'] = $activity_type;
        }

        if($activity_name){
            $where['title'] = ['like','%'.$activity_name.'%'];
        }
        switch( $activity_status ){
            case 1:
                $where['begin_time'] = ['>',$now_time];
                break;
            case 2:
                $where['end_time'] = ['>',$now_time];
                break;
            case 3:
                $where['end_time'] = ['<', $now_time];
                $where['begin_time'] = ['>', $now_time];
                break;
        }
        $order = 'id desc';
        $fileds = 'id,title_img,title,begin_time,end_time,address,max_age,min_age,tel,content ';
        $where['status']= 0;
        $where['is_audit']= 1;
        $list = db('activity_cms')->field($fileds)->where($where)->order($order)->paginate(5);
        $this->assign('list', $list);
        //活动类型
        $activity_typ= get_activity_type();
        //活动区域
        $activity_area = $this->get_area();
        dump($list);
        $this->assign('activity_type', $activity_typ);
        $this->assign('activity_area', $activity_area);
        return $this->fetch();

    }

    //项目简介
    public function show(){
        return $this->fetch('show');
    }

    //区域分级
    protected function get_area()
    {
        //查询一级二级区域
        $where['status'] = 0;
        $where['type'] = 1;
        $fieds = 'id,title,pid';
        $data = db('area')->field($fieds)->where($where)->select();
        foreach($data as $value){
            if($value['pid'] ==0){
                $value['list'] = [];
                $v_1[$value['id']] = $value;
                $v_1_pid[] = $value['id'];
            }else{
                $v_2[$value['id']] = $value;
            }
        }
        foreach($v_2 as $value){
            if( in_array( $value['pid'], $v_1_pid)){
                $value['list'] = [];
                $v_1[$value['id']] = $value;
            }else{
                $v_3[] = $value;
            }
        }
        foreach($v_3 as $v){
            $v_1[$v['pid']]['list'][] = $v;
        }

        return $v_1;
    }



}
