<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

//活动6位随机字符
function get_str($num){
    if($num <= 1){
        return false;
    }
    $str = '1234567890qwertyuiopasdfghjklzxcvbnm';
    $tmp = '';
    for($i=0;$i<$num;$i++){
        $max = strlen($str);
        $sub_num = rand(1,$max);
        $tmp .= substr($str,$sub_num,1);
    }
    return $tmp;

}

//获取项目区域
function get_area( $area_id ){

    $area_where['status'] = 0;
    $area_where['type'] = 1;
    $area_data = db('area')->where("id in ($area_id) or pid=$area_id ")->where($area_where)->field('id,title,pid')->select();
    if( !empty($area_data) ){

        foreach($area_data as $value){
            if( $value['pid'] == 0){
                continue;
            }
            $pids[] = $value['id'];
        }

        if(!empty($pids)){
            $area_where['pid'] = ['in',$pids];
            $tmp = db('area')->where($area_where)->field('id,title,pid')->select();
            if($tmp){
                $area_data = array_merge($area_data, $tmp);
            }
        }
    }

    return $area_data;
}

/**管理员密码加密
 * @param $pwd
 * @param $safe
 */
function admin_password($pwd, $safe){

    return md5(md5($pwd).$safe);
}
/**
 * 获取管理员负责区域
 */
function get_admin_area(){
    //查询一级二级区域
    $where['status'] = 0;
    $where['type'] = 2;
    $pid = db('area')->where($where)->where('pid','=',0)->select();
    $pids = array_column($pid, 'id');
    $p = db('area')->where($where)->where('pid','In',$pids)->select();
    $data = array_merge($pid, $p);
    return $data;
}
/**
 * 获取项目区域
 */
function get_activty_area(){
    //查询一级二级区域
    $where['status'] = 0;
    $where['type'] = 1;
    $pid = db('area')->where($where)->where('pid','=',0)->select();
    $pids = array_column($pid, 'id');
    $p = db('area')->where($where)->where('pid','In',$pids)->select();
    $data = array_merge($pid, $p);
    return $data;
}

function get_activity_type(){
    $activity_typ= db('activity_type')->where('status =0')->field('id,title')->select();
    return $activity_typ;
}