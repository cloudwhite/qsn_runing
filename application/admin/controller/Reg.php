<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Session;
use think\helper\Time;

class Reg extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function add()
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del($id)
    {
        //
    }

    //管理员登录验证
    public function login()
    {
        $ajax['status'] = -1;
        $login_name = Request::instance()->param('login_name');
        $login_pwd= Request::instance()->param('login_pwd');
        $verity= Request::instance()->param('verity');
        if(!captcha_check($verity)){
            //验证失败
            $ajax['status'] = -2;
            $ajax['msg'] = '验证码错误';
            return $this->ajaxData($ajax);
        };
        $where['login_name'] = $login_name;
        //$pwd = md5(md5('oppo123456').'abcdef');
        $userInfo = db('sys_user')->where($where)->find();
        if($userInfo){
            $pwd = md5(md5($login_pwd).$userInfo['safe']);
            if($pwd != $userInfo['password']){
                $ajax['msg'] = '密码错误';
                return $this->ajaxData($ajax);
            }
            if($userInfo['status'] == 1){
                $ajax['msg'] = '用户已禁用';
                return $this->ajaxData($ajax);
            }
            if($userInfo['status'] == -1){
                $ajax['msg'] = '用户不存在';
                return $this->ajaxData($ajax);
            }
            Session::set('admin_id',$userInfo['id']);
            Session::set('power_group_id',$userInfo['power_group_id']);
            Session::set('area_id',$userInfo['area_id']);
            Session::set('name',$userInfo['name']);
            Session::set('head_img',$userInfo['head_img']);
            $ajax['msg'] = '登录成功';
            $ajax['status'] =1;

            $this->SetMenu();

            return $this->ajaxData($ajax);

        }else{
            $ajax['msg'] = '用户不存在';
            return $this->ajaxData($ajax);
        }
        return $this->ajaxData($ajax);

    }


    /**
     * 登出
     */
    public function logout(){
        Session::clear();
        $this->redirect('Index/index');
    }
}
