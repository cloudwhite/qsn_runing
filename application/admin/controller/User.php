<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Session;


class User extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }


    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $title = Request::instance()->param('title');
        if($title){
            $where['title'] = ['like',"%".$title."%"];
        }
        $this->assign('title', $title);
        $request = Request::instance();
        $c = $this->get_c( $request->controller() );
        $where['status'] =['in',[0,1]];
        $order = 'id desc';
        $list = db('activity_type')->where($where)->order($order)->paginate(15);
        $this->assign('actions',$c);

        $this->assign('list',$list);
        return $this->fetch();
    }



    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }


}
