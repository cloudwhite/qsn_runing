<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Session;

class Access extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
        $title = Request::instance()->param('title');
        if($title){
            $where['a.describe'] = ['like',"%".$title."%"];
        }
        $this->assign('title',$title);
        $where['a.status'] =['in',[0,1]];
        $order = 'a.id desc';
        $fileds = 'a.id,a.describe as title,a.action_function,b.describe as pt,a.status,a.ctime';
        $list = db('group_access')
                ->alias('a')
                ->join('qsn_group_access b','a.menu_id = b.id')
                ->field($fileds)
                ->where($where)->order($order)->paginate(15);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function add()
    {
        //
        if( Request::instance()->param('describe') ){

            $access['describe'] =  Request::instance()->param('describe');
            $access['type'] =  Request::instance()->param('type');
            $access['menu_id'] =  Request::instance()->param('menu_id');
            $access['pid'] =  Request::instance()->param('pid') ? Request::instance()->param('pid'): 0;
            $access['action_function'] =  Request::instance()->param('action_function');
            $access['ctime'] = time();
            $access['admin_id'] = Session::get('admin_id');
            $res_log = db('group_access')->insertGetId($access);
            if($res_log){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                $this->success('添加成功',url('Access/index'));
                exit;
            }else{
                $this->error('新增失败');
            }

        }
            //查询菜单
            $where['status'] =['in',[0,1]];
            $where['type'] = 2;
            $where['menu_id'] = 0;
            $menu_1 = db('group_access')->where($where)->select();
            $menu_pids = false;
            foreach($menu_1 as $value){
                $menu_pids[] = $value['id'];
            }
            unset($where['menu_id']);
            if($menu_pids){
                $where['menu_id'] = ['in',$menu_pids];
            }
            $menu_2 = db("group_access")->where($where)->select();
            $menu = array_merge($menu_1,$menu_2);
            $this->assign('menu',$menu);
            return $this->fetch();

    }

    //编辑
    public function edit()
    {

        if( Request::instance()->param('u_id') ){
            $u_id = Request::instance()->param('u_id');
            $access['describe'] =  Request::instance()->param('describe');
            $access['type'] =  Request::instance()->param('type');
            $access['menu_id'] =  Request::instance()->param('menu_id');
            $access['pid'] =  Request::instance()->param('pid');
            $access['action_function'] =  Request::instance()->param('action_function');
            $res = db('group_access')->where('id','=',$u_id)->update($access);
            if($res){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                $this->success('更新成功',url('Access/index'));
            }else{
                $this->error('更新失败');
            }
        }
        $id = Request::instance()->param('id');
        if($id){
            $a_data = db('group_access')->find($id);
            if(empty($a_data)){
                $this->error('信息错误');
            }
            $this->assign('access',$a_data);
        }else{
            $this->error('id错误');
        }

        //查询菜单
        $where['status'] =['in',[0,1]];
        $where['type'] = 2;
        $where['menu_id'] =0;
        $menu_1 = db('group_access')->where($where)->select();
        foreach($menu_1 as $value){
            $menu_pids[] = $value['id'];
        }
        unset($where['menu_id']);
        if($menu_pids){
            $where['menu_id'] = ['in',$menu_pids];
        }
        $menu_2 = db("group_access")->where($where)->select();
        $menu = array_merge($menu_1, $menu_2);
        $this->assign('menu',$menu);
        return $this->fetch();
    }

    //删除
    public function del(){
        $id = Request::instance()->param('id');
        $status = Request::instance()->param('status');
        $ajax['status'] =-1;
        $ajax['msg'] = '删除失败';
        $a_res = Db::execute("update qsn_group_access set status=:status where id=:id",['status'=>$status,'id'=>$id]);
        if($a_res >0){
            $ress = Request::instance();
            $this->relog($ress->controller(),$ress->action());
            $ajax['status'] =1;
            $ajax['msg'] = '删除成功';
        }
        return $this->ajaxData($ajax);
    }
}
