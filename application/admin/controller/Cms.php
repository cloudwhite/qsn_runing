<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Session;

class Cms extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $title = Request::instance()->param('title');
        if($title){
            $where['a.title'] = ['like',"%".$title."%"];
        }
        $this->assign('title', $title);
        $request = Request::instance();
        $c = $this->get_c( $request->controller() );
        $where['a.status'] = 1;
        $fileds = 'a.id,a.title_img,a.title,a.ctime,b.name';
        $order = 'a.id desc';
        $list = db('cms')
                ->alias('a')
                ->join( 'qsn_sys_user b','a.add_user_id=b.id')
                ->where($where)->field($fileds)->order($order)->paginate(10);
        $this->assign('actions',$c);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function add()
    {
        if( Request::instance()->param('title') ){
            $data['title'] =  Request::instance()->param('title');
            $data['content'] =  Request::instance()->param('content');
            $data['add_user_id'] = Session::get('admin_id');
            $data['ctime'] = time();
            $file = request()->file('title_img');
            $info = $file->validate(['size'=>16054272,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
            if($info){
                // 成功上传后 获取上传信息
                $arr = array("\\");
                $arr2 = array("/");
                $data['title_img'] = str_replace($arr, $arr2, $info->getSaveName() );
            }else{
                // 上传失败获取错误信息
                return $this->error($file->getError()) ;
            }

            $id = db('cms')->insertGetId($data);
            if($id){
                $ress = Request::instance();
                $this->relog($ress->controller(), $ress->action());
                $this->success('新增成功',url('Cms/index'));

            }else{

                $this->error('新增失败');
            }
        }
        return $this->fetch();
    }



    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        if( $a_id = Request::instance()->param('a_id') ) {
            $cms['title'] = Request::instance()->param('title');
            $cms['content'] = Request::instance()->param('content');
            $file = request()->file('title_img');
            if( !empty($file) ){
                $info = $file->validate(['size'=>16054272,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
                if($info){
                    // 成功上传后 获取上传信息
                    $arr = array("\\");
                    $arr2 = array("/");
                    $cms['title_img'] = str_replace($arr,$arr2,$info->getSaveName());
                }else{
                    // 上传失败获取错误信息
                    return $this->error($file->getError()) ;
                }

            }
            $res_id = db('activity_cms')->where('id','=',$a_id)->update($cms);
            if($res_id){
                $ress = Request::instance();
                $this->relog($ress->controller(), $ress->action());
                $this->success('更新成功',url('Activity/index'));

            }else{

                $this->error('更新失败');
            }
        }
        $id = Request::instance()->param('id');
        $data = db('cms')->find($id);
        $this->assign('a_data', $data);
        return $this->fetch();
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del()
    {
        $id = Request::instance()->param('id');
        $status =  Request::instance()->param('status');
        $ajax['status'] = -1;
        $ajax['msg'] = '删除失败';
        $u['status'] = $status;
        $res = db('cms')->where('id', '=', $id)->update($u);
        if( $res ){
            $ajax['status'] = 1 ;
            $ajax['msg'] = '删除成功';
            $ress = Request::instance();
            $this->relog($ress->controller(),$ress->action());
        }

        return $this->ajaxData($ajax);
    }
}
