<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Session;

class Area extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $title = Request::instance()->param('title');
        if($title){
            $where['title'] = ['like',"%".$title."%"];
        }
        $this->assign('title', $title);
        $request = Request::instance();
        $c = $this->get_c( $request->controller() );
        $where['status'] =['in',[0,1]];
        $order = 'id desc';
        $list = db('area')->where($where)->order($order)->paginate(15);
        $this->assign('actions',$c);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function add()
    {
        if ( Request::instance()->param('title') ){
            $a['title'] =  Request::instance()->param('title');
            $a['type'] =  Request::instance()->param('type');
            $a['pid'] =  Request::instance()->param('pid');
            $a['ctime'] =  time();
            $a['add_user'] = Session::get('admin_id');
            $id = db('area')->insertGetId($a);
            if( $id ){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                $this->success('新增成功', url('Area/index'));
            }else{
                $this->error('新增失败');
            }
           // $a['']
        }
        //查询一级区域
        $area = get_activty_area();
        $admin_area = get_admin_area();
        $this->assign('a', $area);
        $this->assign('b', $admin_area);
        return $this->fetch();
    }




    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        if( $a_id = Request::instance()->param('a_id') ){
            $a['title'] =  Request::instance()->param('title');
            $a['type'] =  Request::instance()->param('type');
            $a['pid'] =  Request::instance()->param('pid');
            $res = db('area')->where('id', '=', $a_id)->update($a);
            if( $res ){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                $this->success('更新成功', url('Area/index'));
            }else{
                $this->error('更新失败');
            }
        }
        $id = Request::instance()->param('id');
        $data = db('area')->find($id);
        //查询一级区域
        $area = get_activty_area();
        $admin_area = get_admin_area();
        foreach($area as $key=>$value){
            if($id == $value['id']){
                unset($area[$key]);
            }
        }
        foreach($admin_area as $key=>$value){
            if($id == $value['id']){
                unset($admin_area[$key]);
            }
        }
        $this->assign('a', $area);
        $this->assign('b', $admin_area);
        $this->assign('area', $data);
        return $this->fetch();

    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del()
    {
        $id = Request::instance()->param('id');
        $status =  Request::instance()->param('status');
        $ajax['status'] = -1;
        $ajax['msg'] = '删除失败';
        $u['status'] = $status;
        $res = db('area')->where('id', '=', $id)->update($u);
        if( $res ){
            $ajax['status'] = 1 ;
            $ajax['msg'] = '删除成功';
            $ress = Request::instance();
            $this->relog($ress->controller(),$ress->action());
        }

        return $this->ajaxData($ajax);
    }


}
