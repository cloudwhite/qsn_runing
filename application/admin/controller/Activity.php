<?php

namespace app\admin\controller;

use app\admin\model\ActivityComment;
use think\Controller;
use think\Db;
use think\Request;
use think\Session;

class Activity extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //查询区域
        $area_id = Session::get('area_id');
        if(empty($area_id)){
            $area_id =1;
        }
        $status = (int)Request::instance()->param('search');
        if( $status ){
            switch( $status ){
                case 2:
                    $where['a.is_audit'] = array('IN',[-1,0,1]);
                    break;
                case -1:
                    $where['a.is_audit'] = -1;
                    break;
                case -2:
                    $where['a.is_audit'] = 0;
                    break;
                case 1:
                    $where['a.is_audit'] =1;
                    break;
                default:
                    $where['a.is_audit'] = array('IN',[-1,0,1]);
                    break;
            }
        }
        $request = Request::instance();
        $c = $this->get_c( $request->controller() );
        $this->assign('actions',$c);
        $area_data = get_area($area_id);
        $where['a.area_id'] = array('In',array_column($area_data,'id') );
        $where['a.status'] = 1;
        $fileds = 'a.id as a_id,a.is_show,a.title,a.ctime,b.name,a.tel,a.is_audit,d.title as type,c.title as area';
        $order = 'a.is_show desc, a.id desc ';
        $list = db('activity_cms')
                ->alias('a')
                ->join( 'qsn_sys_user b','a.add_user=b.id')
                ->join('qsn_area c','a.area_id=c.id')
                ->join('qsn_activity_type d','a.type_id=d.id')
                ->where($where)->field($fileds)->order($order)->paginate(15);
        //dump($list);

        $this->assign('list',$list);
        return $this->fetch();

    }

    //新建
    public function add()
    {
        //查询区域
        $area_id = Session::get('area_id');
        $area_id =1;
        $area_data = get_area($area_id);
        //dump($area_data);
        if( Request::instance()->param('title') ){
            $activity['title'] = Request::instance()->param('title');
            $activity['area_id'] = Request::instance()->param('area_id');
            $activity['type_id'] = Request::instance()->param('type_id');
            $activity['begin_time'] = strtotime( Request::instance()->param('begin_time') );
            $activity['end_time'] = strtotime( Request::instance()->param('end_time') );
            $activity['min_age'] = Request::instance()->param('min_age');
            $activity['max_age'] = Request::instance()->param('max_age');
            $activity['own_user'] = Request::instance()->param('own_user');
            $activity['tel'] = Request::instance()->param('tel');
            $activity['content'] = Request::instance()->param('content');
            $activity['add_user'] = Session::get('admin_id');
            $activity['address'] = Request::instance()->param('address');
            $activity['ctime'] = time();
            $file = request()->file('title_img');
            $info = $file->validate(['size'=>16054272,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
            if($info){
                // 成功上传后 获取上传信息
                $arr = array("\\");
                $arr2 = array("/");
                $activity['title_img'] = str_replace($arr,$arr2,$info->getSaveName());
            }else{
                // 上传失败获取错误信息
                return $this->error($file->getError()) ;
            }

            $id = db('activity_cms')->insertGetId($activity);
            if($id){
                $ress = Request::instance();
                $this->relog($ress->controller(), $ress->action());
                $this->success('添加项目成功',url('Activity/index'));

            }else{

                $this->error('新增项目失败');
            }

        }

        $activity_type = db('activity_type')->where('status =0')->select();
        $this->assign('area_data',$area_data);
        $this->assign('activity_type',$activity_type);
        return $this->fetch('add');

    }

    //审核
    public function audit($id){
        $a = db('activity_cms')
             ->alias('a')
             ->join('qsn_activity_type b','a.type_id = b.id')
             ->field('a.*,b.title as type')
             ->find($id);
        $this->assign('a_data',$a);
        return $this->fetch();
    }


    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {

        if( $a_id = Request::instance()->param('a_id') ) {
            $activity['title'] = Request::instance()->param('title');
            $activity['area_id'] = Request::instance()->param('area_id');
            $activity['type_id'] = Request::instance()->param('type_id');
            $activity['begin_time'] = strtotime(Request::instance()->param('begin_time'));
            $activity['end_time'] = strtotime(Request::instance()->param('end_time'));
            $activity['min_age'] = Request::instance()->param('min_age');
            $activity['max_age'] = Request::instance()->param('max_age');
            $activity['own_user'] = Request::instance()->param('own_user');
            $activity['tel'] = Request::instance()->param('tel');
            $activity['content'] = Request::instance()->param('content');
            $activity['address'] = Request::instance()->param('address');
            $file = request()->file('title_img');
            if( !empty($file) ){
                $info = $file->validate(['size'=>16054272,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
                if($info){
                    // 成功上传后 获取上传信息
                    $arr = array("\\");
                    $arr2 = array("/");
                    $activity['title_img'] = str_replace($arr,$arr2,$info->getSaveName());
                }else{
                    // 上传失败获取错误信息
                    return $this->error($file->getError()) ;
                }

            }
            $res_id = db('activity_cms')->where('id','=',$a_id)->update($activity);
            if($res_id){
                $ress = Request::instance();
                $this->relog($ress->controller(), $ress->action());
                $this->success('更新成功',url('Activity/index'));

            }else{

                $this->error('更新失败');
            }
        }

        $id = Request::instance()->param('id');
        $area_id = Session::get('area_id');
        $area_data = get_area($area_id);
        $data = db('activity_cms')->find($id);
        $activity_type = db('activity_type')->where('status =0')->select();
        $this->assign('area_data',$area_data);
        $this->assign('activity_type',$activity_type);
        $this->assign('a_data',$data);

        return $this->fetch();
    }

    //审核操作
    public function set_aduit()
    {

        $id = Request::instance()->param('id');
        $is_audit = Request::instance()->param('audit');
        $audit_content = Request::instance()->param('audit_content');
        $ajax['status'] =-1;
        $ajax['msg'] ='操作不成功';
        $u['is_audit'] = $is_audit;
        $u['audit_idea'] = $audit_content;
        $a_res = db("activity_cms")->where('id', '=', $id)->update($u);
        if($a_res){
            $ress = Request::instance();
            $this->relog($ress->controller(), $ress->action());
            $ajax['status'] = 1;
            $ajax['msg'] = '操作成功';
        }
        return $this->ajaxData($ajax);


    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del()
    {
        $id = Request::instance()->param('id');
        $status =  Request::instance()->param('status');
        $ajax['status'] = -1;
        $ajax['msg'] = '删除失败';
        $u['status'] = $status;
        $res = db('activity_cms')->where('id', '=', $id)->update($u);
        if( $res ){
            $ajax['status'] = 1 ;
            $ajax['msg'] = '删除成功';
            $ress = Request::instance();
            $this->relog($ress->controller(),$ress->action());
        }

        return $this->ajaxData($ajax);
    }

    /**
     * 设置项目置顶状态
     */
    function set_show ()
    {
        $id = Request::instance()->param('id');
        $status = Request::instance()->param('status');
        if($status == -1){
            $msg = '取消推荐';
        }elseif($status == 1){
            $msg = '推荐';
        }
        $ajax['msg'] = $msg.'失败';
        $ajax['status'] = -1;
        $u['is_show'] = $status;
        $res = db('activity_cms')->where('id','=',$id)->update($u);
        if( $res ){
            $ress = Request::instance();
            $this->relog($ress->controller(), $ress->action());
            $ajax['status'] = 1;
            $ajax['msg'] = $msg.'成功';
        }

        return $this->ajaxData($ajax);

    }

    /**
     * 用户项目报名表
     */
    public function user_list()
    {
        $title = Request::instance()->param('title');
        if($title){
            $where['b.name'] = ['like',"%".$title."%"];
        }
        $id = Request::instance()->param("id");
        $this->assign('title', $title);
        $activity = db('activity_cms')->find($id);
        $this->assign('activity', $activity);
        $fileds = 'a.id,a.ctime,b.name,b.mobile,c.title';
        $order = 'a.id desc';
        $where['a.status'] = 0;
        $list = db('activity_user')
                ->alias('a')
                ->join('qsn_user b', 'a.user_id = b.id')
                ->join('qsn_activity_cms c', 'a.activity_id = c.id')
                ->where($where)->field($fileds)->order($order)->paginate(15);
        $this->assign('list',$list);
        return $this->fetch();
    }

    //取消用户参加项目资格
    public function del_user()
    {
        $id = Request::instance()->param('id');
        $status =  Request::instance()->param('status');
        $ajax['status'] = -1;
        $ajax['msg'] = '取消失败';
        $u['status'] = $status;
        $res = db('activity_user')->where('id', '=', $id)->update($u);
        if( $res ){
            $ajax['status'] = 1 ;
            $ajax['msg'] = '取消成功';
            $ress = Request::instance();
            $this->relog($ress->controller(),$ress->action());
        }

        return $this->ajaxData($ajax);
    }

}
