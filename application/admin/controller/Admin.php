<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Session;

class Admin extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $title = Request::instance()->param('title');
        if($title){
            $where['a.name'] = ['like',"%".$title."%"];
        }
        $this->assign('title',$title);
        $request = Request::instance();
        $c = $this->get_c( $request->controller() );
        $where['a.status'] =['in',[0,1]];
        $order = 'a.id desc';
        $fileds = 'a.id,a.login_name, a.add_time,a.name,a.mobile,a.tel,a.status,b.title as group_title,c.title as area_title';
        $list = db('sys_user')
                ->alias('a')
                ->join( 'qsn_power_group b','a.power_group_id=b.id')
                ->join('qsn_area c','a.area_id=c.id')
                ->where($where)->order($order)->field($fileds)->paginate(15);
        //dump($c);
        $this->assign('actions',$c);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function add()
    {

        if( Request::instance()->param('login_name') ){

            $safe =  get_str(6);
            $user['login_name'] = Request::instance()->param('login_name');
            $user['password'] = md5( md5( Request::instance()->param('password') ).$safe );
            $user['safe'] = $safe;
            $user['area_id'] =  Request::instance()->param('area_id') ;
            $user['power_group_id'] = Request::instance()->param('power_group_id') ;
            $user['name'] = Request::instance()->param('name');
            $user['tel'] = Request::instance()->param('tel');
            $user['address'] = Request::instance()->param('address');
            $user['tel'] = Request::instance()->param('tel');
            $user['add_user'] = Session::get('admin_id');
            $user['add_time'] = time();
            $file = request()->file('head_img');
            $info = $file->validate(['size'=>16054272,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
            if($info){
                // 成功上传后 获取上传信息
                $arr = array("\\");
                $arr2 = array("/");
                $user['head_img'] = str_replace($arr,$arr2,$info->getSaveName());
            }else{
                // 上传失败获取错误信息
                return $this->error($file->getError()) ;
            }

            $id = db('sys_user')->insertGetId($user);
            if($id){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                $this->success('新增管理员成功',url('Admin/index'));
            }else{
                $this->error('新增管理员失败');
            }
            exit;
        }
        $p_w['status'] = 0;
        $power_group = db('power_group')->where($p_w)->field('id,title')->select();
        $p_w['type'] = 2;
        $area = db('area')->where($p_w)->field('title,id')->select();
        $this->assign('area_data', $area);
        $this->assign('power_group', $power_group);
        return $this->fetch();
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function stop($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        $id = Request::instance()->param('id');
        $data = db('sys_user')->find($id);
        if( Request::instance()->param('u_id') ){
            $u_id = Request::instance()->param('u_id');
            $update['login_name'] = Request::instance()->param('login_name');
            $update['area_id'] = Request::instance()->param('area_id');
            $update['power_group_id'] = Request::instance()->param('power_group_id');
            $update['name'] = Request::instance()->param('name');
            $update['tel'] = Request::instance()->param('tel');
            $update['address'] = Request::instance()->param('address');
            $pwd = Request::instance()->param('password');
            if( $pwd ){
                $safe = get_str(6);
                $update['safe'] = $safe;
                $update['password'] = md5(md5($pwd).$safe);
            }
            $file = request()->file('head_img');
            if( !empty($file) ){
                $info = $file->validate(['size'=>16054272,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
                if($info){
                    // 成功上传后 获取上传信息
                    $arr = array("\\");
                    $arr2 = array("/");
                    $update['head_img'] = str_replace($arr,$arr2,$info->getSaveName());
                }else{
                    // 上传失败获取错误信息
                    return $this->error($file->getError()) ;
                }
            }

            $res = db('sys_user')->where('id','=',$u_id)->update($update);
            if($res){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                $this->success('更新成功',url('Admin/index'));
            }else{
                $this->error('更新失败');
            }

        }
        $this->assign('UserInfo',$data);
        $p_w['status'] = 0;
        $power_group = db('power_group')->where($p_w)->field('id,title')->select();
        $p_w['type'] = 2;
        $area = db('area')->where($p_w)->field('title,id')->select();
        $this->assign('area_data', $area);
        $this->assign('power_group', $power_group);
        return $this->fetch();

    }


    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del()
    {
        $id = Request::instance()->param('id');
        $ajax['status'] = -1;
        $status = Request::instance()->param('status');
        if($status == 0){
            $msg = '启用';
        }elseif($status == -1){
            $msg = '删除';
        }elseif($status == 1){
            $msg = '禁用';
        }
        $ajax['msg'] = $msg.'失败';
        if( $id == 1 ){
            $ajax['msg'] = '管理员不能'.$msg;
            return $this->ajaxData($ajax);
        }
        $u['status'] = $status;
        $res = db('sys_user')->where('id','=',$id)->update($u);
        if( $res ){
            $ajax['status'] = 1;
            $ajax['msg'] = $msg.'成功';
        }

        return $this->ajaxData($ajax);
    }
}
