<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Type extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $title = Request::instance()->param('title');
        if($title){
            $where['title'] = ['like',"%".$title."%"];
        }
        $this->assign('title', $title);
        $request = Request::instance();
        $c = $this->get_c( $request->controller() );
        $where['status'] =['in',[0,1]];
        $order = 'id desc';
        $list = db('activity_type')->where($where)->order($order)->paginate(15);
        $this->assign('actions',$c);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 新增
     */
    public function add(){

        if( $title = Request::instance()->param('title') ){
            $d['title'] = $title;
            $id = db('activity_type')->insertGetId($d);
            if( $id ){
                $ress = Request::instance();
                $this->relog($ress->controller(), $ress->action());
                $this->success('新增成功', url('Type/index'));
            }else{
                $this->error('新增失败');
            }
        }
        return $this->fetch();

    }



    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        $id = Request::instance()->param('id');
        if( $t_id = Request::instance()->param('t_id') ){

            $t['title'] = Request::instance()->param('title');
            $res = db('activity_type')->where('id', '=', $t_id)->update($t);
            if ( $res ){
                $ress = Request::instance();
                $this->relog($ress->controller(), $ress->action());
                $this->success('更新成功', url('Type/index'));
            }else{
                $this->error('更新失败');
            }
        }
        $t_data = db('activity_type')->find($id);
        $this->assign('t_data', $t_data);
        return $this->fetch();
    }


    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del($id)
    {
        $id = Request::instance()->param('id');
        $status =  Request::instance()->param('status');
        $ajax['status'] = -1;
        $ajax['msg'] = '删除失败';
        $u['status'] = $status;
        $res = db('activity_type')->where('id', '=', $id)->update($u);
        if( $res ){
            $ajax['status'] = 1 ;
            $ajax['msg'] = '删除成功';
            $ress = Request::instance();
            $this->relog($ress->controller(),$ress->action());
        }

        return $this->ajaxData($ajax);
    }
}
