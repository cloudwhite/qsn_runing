<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Session;

class Power extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->_admin();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $title = Request::instance()->param('title');
        if($title){
            $where['title'] = ['like',"%".$title."%"];
        }
        $this->assign('title', $title);
        $request = Request::instance();
        $c = $this->get_c( $request->controller() );
        $where['status'] =['in',[0,1]];
        $order = 'id desc';
        $list = db('area')->where($where)->order($order)->paginate(15);
        $this->assign('actions',$c);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function add()
    {

        if( Request::instance()->param('title') ){
            $data['title'] = Request::instance()->param('title') ;
            $ids =  Request::instance()->param('action_ids/a');
            $where['id'] = ['in',$ids];
            //查找菜单选项
            $p_ids = db('group_access')->where($where)->field('menu_id')->select();
            $m_id = array_unique( array_column($p_ids,'menu_id') );
            $where['id'] = ['in',$m_id];
            $where['menu_id'] = 0;
            $m_ids = db('group_access')->where($where)->field('id')->select();
            $m_p_id = array_unique( array_column($m_ids,'id') ) ;
            $power_ids = array_merge($ids,$m_p_id);
            $data['ctime'] = time();
            $data['action_ids'] = implode(',',$power_ids);
            $data['admin_id'] = Session::get('admin_id');
            $res = db('power_group')->insertGetId($data);
            if($res){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                return $this->success('权限组创建成功',url('Power/index'));
            }else{
                $this->error('创建失败');
            }


        }

        //查询数据
        $data = db('group_access')->where('status =0')->select();
        foreach($data as $v_1){
            if($v_1['type'] ==2 && $v_1['menu_id'] ==0){
                $v_m_1[] = $v_1['id'];
            }else{
                $m[] = $v_1;
            }
        };
        $m_2 = [];
        $m_3 = [];
        foreach($m as $m_1){
            if($m_1['type'] ==2 && in_array($m_1['menu_id'],$v_m_1)){
                $m_2[$m_1['id']] =$m_1;
                $m_2[$m_1['id']]['actions'] =[];
            }else{
                $m_3[] = $m_1;
            }
        }
        foreach($m_3 as $v_3){
            $m_2[$v_3['menu_id']]['actions'][] = $v_3;
        }
        $this->assign('menu',$m_2);
        return $this->fetch();
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        if( Request::instance()->param('title') ){
            $data['title'] = Request::instance()->param('title') ;
            $ids =  Request::instance()->param('action_ids/a');
            $where['id'] = ['in',$ids];
            //查找菜单选项
            $p_ids = db('group_access')->where($where)->field('menu_id')->select();
            $m_id = array_unique( array_column($p_ids,'menu_id') );
            $where['id'] = ['in',$m_id];
            $where['menu_id'] = 0;
            $m_ids = db('group_access')->where($where)->field('id')->select();
            $m_p_id = array_unique( array_column($m_ids,'id') ) ;
            $power_ids = array_merge($ids,$m_p_id);
            $g_id =  Request::instance()->param('g_id');
            $data['action_ids'] = implode(',',$power_ids);
            $data['admin_id'] = Session::get('admin_id');
            $res = db('power_group')->where('id','=',$g_id)->update($data);
            if($res){
                $ress = Request::instance();
                $this->relog($ress->controller(),$ress->action());
                return $this->success('更新成功',url('Power/index'));
            }else{
                $this->error('更新失败');
            }
        }
        $request = Request::instance();
        //echo $request->controller();
        //echo $request->action();
        $id = Request::instance()->param('id');
        $data = db("power_group")->find($id);
        //dump($data);
        $m_ids = explode(",",$data['action_ids']);
        $menus = $this->get_menus($m_ids);
        $this->assign('p_data', $data);
        $this->assign('menu', $menus );

        return $this->fetch();

    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del($id)
    {
        //
        $id = Request::instance()->param('id');
        $status = Request::instance()->param('status');
        $ajax['status'] = -1;
        $ajax['msg'] = '删除失败';
        $p_res = db('sys_user')->where('power_group_id','=',$id)->find();
        if($p_res){
            $ajax['msg'] = '请删除关联用户';
            return $this->ajaxData($ajax);
        }
        $a_res = Db::execute("update qsn_power_group set status=:status where id=:id",['status'=>$status,'id'=>$id]);
        if($a_res >0){
            $ress = Request::instance();
            $this->relog($ress->controller(),$ress->action());
            $ajax['status'] =1;
            $ajax['msg'] = '删除成功';
        }
        return $this->ajaxData($ajax);

    }
    function get_menus($p_ids){

        if( !is_array($p_ids) ){
            return false;
        }

        //查询数据
        $data = db('group_access')->where('status =0')->select();
        foreach($data as &$v_1){

            if( in_array($v_1['id'], $p_ids) ){
                    $v_1['checked'] = 'checked';
            }else{
                    $v_1['checked'] = '';
            }
            if($v_1['type'] ==2 && $v_1['menu_id'] ==0){
                $v_m_1[] = $v_1['id'];
            }else{
                $m[] = $v_1;
            }
        };
        $m_2 = [];
        $m_3 = [];
        foreach($m as $m_1){
            if($m_1['type'] ==2 && in_array($m_1['menu_id'],$v_m_1)){
                $m_2[$m_1['id']] =$m_1;
                $m_2[$m_1['id']]['actions'] =[];
            }else{
                $m_3[] = $m_1;
            }
        }
        foreach($m_3 as $v_3){
            $m_2[$v_3['menu_id']]['actions'][] = $v_3;
        }

        return $m_2;
    }
}
