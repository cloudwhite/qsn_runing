<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
//系统常量声明

defined('HOST') or define('HOST', $_SERVER['HTTP_HOST'] );
defined('STATIC_PATH') or define('STATIC_PATH', 'http://'.HOST .'/qsn_runing/public');
defined('URL_PIC') or define('URL_PIC', STATIC_PATH .'/'.'uploads/'); // 图片常量
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
