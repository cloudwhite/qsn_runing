<?php
// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
//系统常量声明
defined('HOST') or define('HOST', $_SERVER['HTTP_HOST'] );
defined('STATIC_PATH') or define('STATIC_PATH', 'http://'.HOST .'/qsn_runing/public');
defined('STATIC_JS') or define('STATIC_JS', STATIC_PATH .'/static/admin/js/');
defined('STATIC_IMAGE') or define('STATIC_IMAGE', STATIC_PATH .'/static/admin/images/');
defined('STATIC_CSS') or define('STATIC_CSS', STATIC_PATH .'/static/admin/css/');
defined('STATIC_IMG_URL') or define('STATIC_IMG_URL', STATIC_PATH .'/uploads/');
defined('URL_PIC') or define('URL_PIC', STATIC_PATH .'/'.'uploads/'); // 图片常量
// 加载框架引导文件
define('BIND_MODULE','admin');
require __DIR__ . '/../thinkphp/start.php';
